#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
from os import path
try:
	from dotenv import load_dotenv
except ModuleNotFoundError:
	print("A dependency is missing. Run: pip install -r requirements.txt")
	exit(1)



# ==============================================================================
#                                     GLOBAL
# ==============================================================================
DATA_DIR = path.dirname(path.realpath(__file__))
APP_DIR = path.dirname(path.realpath(__file__))

# Load .env
if path.exists(path.join(APP_DIR,".env")):
	load_dotenv()
	SECRET_KEY = os.getenv("SECRET_KEY")
	DEBUG = os.getenv("DEBUG")
else:
	import secrets	
	SECRET_KEY = secrets.token_hex(20)
	DEBUG = False
	with open(".env", "w") as f:
		f.write(f"""
SECRET_KEY={SECRET_KEY}
DEBUG={DEBUG}
""")

# ==============================================================================
#                                   WEBSERVER
# ==============================================================================
PORT="8383"
STATIC_DIR = path.join(APP_DIR, "Webapp", "static")
TEMPLATE_DIR = path.join(APP_DIR, "Webapp", "templates")

# ==============================================================================
#                                TEMPLATE ENGINE
# ==============================================================================
JINJA_TRIM_BLOCKS = True
JINJA_LSTRIP_BLOCKS = True
# ==============================================================================
#                                    DATABASE
# ==============================================================================
DATABASE_NAME = "data.db"
SQLALCHEMY_DATABASE_URI = "sqlite:///" + path.join(DATA_DIR, DATABASE_NAME)
SQLALCHEMY_TRACK_MODIFICATIONS = DEBUG

