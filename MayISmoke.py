#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import argparse

parser = argparse.ArgumentParser()

# ==============================================================================
#                                  OPTIONS LIST
# ==============================================================================
parser.add_argument("-w", "--web",
					help="Start the app as a webserver",
					action="store_true")

parser.add_argument("-m", "--migrate",
					help="Migrate database",
					action="store_true")

parser.add_argument("-i", "--install",
					help="Launch installation script",
					action="store_true")

args = parser.parse_args()

# ==============================================================================
#                                  ENTRY POINTS
# ==============================================================================
if args.web:
	import Settings
	from Webapp import create_app
	app = create_app()
	app.run(host="127.0.0.1", port=Settings.PORT, debug=Settings.DEBUG)

elif args.migrate:
	import subprocess as sp
	session = sp.Popen(["./ServerUtils/MigrateDatabase.sh"], stdout=sp.PIPE, stderr=sp.PIPE)
	stdout, stderr = session.communicate()
	print(stdout.decode("UTF-8"))
	if stderr:
		print(stdout.decode("UTF-8"))
	
elif args.install:
	import subprocess as sp
	session = sp.Popen(["./ServerUtils/Install.sh"], stdout=sp.PIPE, stderr=sp.PIPE)
	stdout, stderr = session.communicate()
	print(stdout.decode("UTF-8"))
	if stderr:
		print(stdout.decode("UTF-8"))

else:
	print("⚠️ At least one option is required.\n")
	parser.print_help()
	exit(1)
