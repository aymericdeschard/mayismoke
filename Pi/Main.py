#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
from time import sleep
import RPi.GPIO as io

from Led import Led
from Button import ButtonHandler

io.setmode(io.BOARD)
io.setup(16, io.IN, pull_up_down=io.PUD_UP)

red = 0
green = 100

led = Led()

def pressed():
	led.turn_on()
	led.set_color([red, green, 0])
	red = green
	green = red

btn = ButtonHandler(16, pressed, edge='rising', bouncetime=100)
btn.start()
io.add_event_detect(16, io.RISING, callback=btn)

while True:
	continue	
