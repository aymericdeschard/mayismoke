#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from time import sleep
import RPi.GPIO as io

class Led():
	def __init__(self, redPin=13, greenPin=11, bluePin=15, frequency=100):
		io.setmode(io.BOARD)
		self.pins = [redPin, greenPin, bluePin]
		# pins as outputs
		io.setup(self.pins, io.OUT)
		# Initialize the channels objects
		self.channels = self.set_frequency(frequency)
		# Brithness level
		self.bright_coef = 1.
		# Colors
		self.colors = [100, 100, 100]
		# Set of the LED, either hard or soft on/off		
		self.stateOn = False

	def __del__(self):
		self.set_color([100, 100, 100])
		self.turn_off()
		io.cleanup(self.pins)

	def __str__(self):
		return f"""RGB LED on pins {self.pins}
	Turned on: {self.stateOn}
	RGB colors: {self.colors}
	Brightness level: {self.bright_coef * 100}%"""

	def set_frequency(self, frequency):
		"""Set the frequency of all colors chanels in hertz"""

		channels = list()
		for pin in self.pins:
			p = io.PWM(pin, frequency)
			channels.append(p)

		return channels

	def set_brightness(self, percent):
		"""Dim or brighten the LED on a scale from 0 to 100"""

		self.bright_coef = percent / 100
		for chan in self.channels:
			chan.ChangeDutyCycle(100 * self.bright_coef)

	def set_color(self, rgb):
		"""Set the color of the LED on RGB scale from 0 to 100"""

		self.colors = rgb
		for idx, chan in enumerate(self.channels):
			chan.ChangeDutyCycle(rgb[idx] * self.bright_coef)

	def invert_state(self):
		""" Reverse the state of the led from on to off without changing
			its color value or brightness"""

		for idx, chan in enumerate(self.channels):
			if self.stateOn:
				chan.ChangeDutyCycle(0)
				self.stateOn = False
			else:
				chan.ChangeDutyCycle(self.colors[idx] * self.bright_coef)
				self.stateOn = True

	def cycle_colors(self, whichRgb, coef):
		""" Increment or decrement the color value for each channel set

			Args:
				whichRgb <list> : the channels to set in RGB order
					 0: no change
					 1: increment
					-1: decrement

				coef <int>: the value from 0 to 100 to increment or decrement
		"""

		for idx, chan in enumerate(self.channels):
			if whichRgb[idx] != 0:
				val = self.colors[idx] + ( coef * whichRgb[idx] )
				if val < 0 or val > 100:
					self.colors[idx] = val % 100
				else:
					self.colors[idx] = val
		self.set_color(self.colors)

	def turn_on(self):
		"""Turn on the LED on all channel"""

		for channel in self.channels:
			channel.start(1)
		self.stateOn = True

	def turn_off(self):
		"""Turn off the LED on all channel"""

		for channel in self.channels:
			channel.start(0)
		self.stateOn = False

	def test_wiring(self):
		"""Test the LED wiring by cycling the colors at full brightness."""

		print("Wiring test started. Colors are going to cycle.")
		self.set_brightness(100)
		self.set_color([0, 0, 0])
		self.turn_on()

		print("Red on")
		self.set_color([100, 0, 0])
		sleep(5)

		print("Green on")
		self.set_color([0, 100, 0])
		sleep(5)

		print("Blue on")
		self.set_color([0, 0, 100])
		sleep(5)

		print("Wiring test finished.")
		self.turn_off()


if __name__ == "__main__":
	io.setmode(io.BOARD)
	led = Led()
	led.test_wiring()

	led.turn_on()

	led.set_brightness(100)
	green = 100
	red = 0
	led.set_color([red, green, 0])
	coef = 10
	while True:
		led.cycle_colors([1, -1, 0], 1)
		print(f"\rRed: {led.colors[0]:02d} Green: {led.colors[1]:02d}", end="")
		sleep(.5)
