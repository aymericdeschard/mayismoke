#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import RPi.GPIO as io
import threading
from time import sleep

class ButtonHandler(threading.Thread):
	def __init__(self, pin, func, edge='both', bouncetime=200):
		super().__init__(daemon=True)

		self.edge = edge
		self.func = func
		self.pin = pin
		self.bouncetime = float(bouncetime)/1000

		self.lastpinval = io.input(self.pin)
		self.lock = threading.Lock()

	def __call__(self, *args):
		if not self.lock.acquire(blocking=False):
			return
		print("call")
		t = threading.Timer(self.bouncetime, self.read, args=args)
		t.start()

	def read(self, *args):
		pinval = io.input(self.pin)

		print("read")
		if (
				((pinval == 0 and self.lastpinval == 1) and
				 (self.edge in ['falling', 'both'])) or
				((pinval == 1 and self.lastpinval == 0) and
				 (self.edge in ['rising', 'both']))
		):
			self.func(*args)

		self.lastpinval = pinval
		self.lock.release()

from Led import Led
io.setmode(io.BOARD)
LED = Led()
GREEN = 100
RED = 0

def real_cb(*args):
	global GREEN, RED, LED
	LED.set_color([RED, GREEN, 0])
	GREEN = abs(GREEN - 100)
	RED = abs(RED - 100)
	

if __name__ == "__main__":
	LED.turn_on()
	LED.set_color([RED, GREEN, 0])
	io.setup(16, io.IN, pull_up_down=io.PUD_UP)
	cb = ButtonHandler(16, real_cb, edge='rising', bouncetime=100)
	cb.start()
	io.add_event_detect(16, io.RISING, callback=cb)
	while True:
		continue
