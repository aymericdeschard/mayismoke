#!/usr/bin/env bash

set -e

sudo apt-get -y install python3 python3-venv
sudo apt-get -y install supervisor nginx 

python3 -m venv ../venv
source ../venv/bin/activate

pip install -r ../requirements.txt

# prod dependencies
pip install gunicorn

# generate db
./MigrateDatabase.sh

# service
sudo cp ./mayismoke.conf /etc/supervisor/conf.d/mayismoke.conf
sudo supervisorctl reload

# webserver
sudo cp ./mayismoke.nginx /etc/nginx/sites-available/mayismoke
sudo ln -s /etc/nginx/sites-available/mayismoke /etc/nginx/sites-enabled/mayismoke

sudo service nginx reload

exit 0
