#!/usr/bin/env bash

source venv/bin/activate

export FLASK_APP="Webapp"

if [ ! -d "migrations" ]; then
	echo "##### Migration initialization #####"
	flask db init
fi

echo "##### Migrating database #####"
flask db migrate

echo "##### Upgrade database #####"
flask db upgrade

exit 0
