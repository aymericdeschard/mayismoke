/* ========================================================================== */
/*                       PARSE THE DATE TO COUNTDOWN TO                       */
/* ========================================================================== */

var datetime = document.getElementById("next-maturity").getAttribute("value").split(" ");
var time = datetime[1].split(":").slice(0, 2);
var date = datetime[0].split("-");

var countDownDate = Date.UTC(
							date[0],
							date[1] - 1, // JS consider month as an array
							date[2],
							time[0],
							time[1]
							);

// Maturity in milliseconds
var maturity = document.getElementById("maturity").innerHTML.split(":");
maturity = (parseInt(maturity[0]) * 60 + parseInt(maturity[1])) * 60 * 1000;
// Set the next maturity with user timezone
var localeTS = new Date(countDownDate);
var hours = localeTS.getHours().toString().padStart(2, "0");
var minutes = localeTS.getMinutes().toString().padStart(2, "0");
var localeString = hours + ":" + minutes;
document.getElementById("next-maturity").innerHTML = localeString;

/* ========================================================================== */
/*                           DOM ELEMENTS TO UPDATE                           */
/* ========================================================================== */
var timeRemaining = document.getElementById("time-remaining");
var progressBar = document.getElementById("progress-bar");

/* ========================================================================== */
/*                         ELEMENTS UPDATE FUNCTIONS                          */
/* ========================================================================== */
function setCountdownTxt (distance){

	// Countdown string for the timeRemaining element
	var cdStr = "";
	
	// Due-time is passed, becomes a timer
	if (distance < 0) {
		distance = Math.abs(distance);
		cdStr += "+";
	}

	// Time calculations for days, hours, minutes and seconds
	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	// String builder for countdown
	// days
	if (days > 0)			cdStr += days + "d";
	// hours
	if (hours < 10) 		cdStr += "0" + hours + ":";
	else 				 	cdStr += hours + ":";
	// minutes
	if (minutes < 10) 		cdStr += "0" + minutes + ":";
	else 					cdStr += minutes + ":";
	// seconds
	if (seconds < 10) 		cdStr += "0" + seconds;
	else 					cdStr += seconds;

	timeRemaining.innerHTML = cdStr;

}

function setCountdownImg(distance) {
	
	if ( distance < 0 ) {
		imgYes.style.display = "block";
		imgNo.style.display = "none";
	} else {
		imgNo.style.display = "block";
		imgYes.style.display = "none";
	}

}

function setProgressBar(progress) {
	progressBar.style.width = progress +"%";
	// Set the timer color to be visible
	// depending on the progress bar size
	if (progress < 50 ){
		progressBar.style.color = "#455262";
	} else {
		progressBar.style.color = "white";
	}
	
	if (progress >= 100){
		progressBar.classList.remove("progress-bar-striped");	
	}
}

function calculateChannel(progress, min, max) {
	/* Caculate the value for a channel on RGB
 	   toward its progress 
 	*/ 
	return Math.round( ( (progress * (max - min)) / 100 ) + min );

}

function setCountdownColor(progress){

	// Color ranges
	// with min when the progress   0%
	// with max when the progress 100%
	var redMax = 0;
	var redMin = 102;

	var greenMax = 102;
	var greenMin = 0;

	var blueMax = 84;
	var blueMin = 18;

	// limit to 100% of the color
	if ( progress > 100 ) { progress = 100; }
	// Calculate the color
	var progressRed   = calculateChannel(progress, redMin, redMax);
	var progressGreen = calculateChannel(progress, greenMin, greenMax);
	var progressBlue  = calculateChannel(progress, blueMin, blueMax);

	var rgb = "rgb(" + progressRed + "," + progressGreen + "," + progressBlue + ")";

	progressBar.style.backgroundColor = rgb;
}

/* ========================================================================== */
/*                                 MAIN LOOP                                  */
/* ========================================================================== */
// Update the countdown every 1 second
var x = setInterval(function() {

	// Get today's date and time
	var now = new Date().getTime();

	// Find the distance between now and the countdown date
	var distance = countDownDate - now;

	// calculate the % of progress
	var startedTime = countDownDate - maturity;
	var progress = ((now - startedTime)/ maturity) * 100;
	setProgressBar(progress);
	// Update the image
	// setCountdownImg(distance);

	// Update button color
	setCountdownColor(progress);

	// Update text
	setCountdownTxt(distance);

}, 1000);
