#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from . import Journals, Stats

def register_blueprints(app):
	app.register_blueprint(Journals.Routes.blueprint)
	app.register_blueprint(Stats.Routes.blueprint, url_prefix="/stats")

def register_models():
	from .Journals import Models


