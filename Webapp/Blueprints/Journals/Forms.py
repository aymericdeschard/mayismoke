#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from flask_wtf import FlaskForm
from wtforms.fields.html5 import IntegerField
from wtforms import SubmitField
from wtforms.validators import InputRequired 

from .Models import MaturitiesJournal, SmokesJournal

class MaturityForm(FlaskForm):
	hours = IntegerField("Hours", validators=[InputRequired()])
	minutes = IntegerField("Minutes", validators=[InputRequired()])
