#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
from datetime import datetime, timedelta

from flask import Blueprint, render_template, redirect, request
import Settings

from . import Models, Forms
from .Models import MaturitiesJournal, SmokesJournal

blueprint = Blueprint(
	"journals",
	__name__,
	static_folder=Settings.STATIC_DIR,
	template_folder="templates"
	)


@blueprint.route("/", methods=["GET"])
def home():
	maturity = MaturitiesJournal.select_last()
	lastSmoked = SmokesJournal.select_last()
	if lastSmoked:
		nextMaturity = lastSmoked.value + timedelta(minutes=maturity.value)
	else:
		nextMaturity = None

	return render_template(
		"journals/index.html",
		nextMaturity = nextMaturity,
		maturity	 = maturity
	)


@blueprint.route("/smoked", methods=["GET"])
def smoked():
	SmokesJournal.create(
		value=datetime.utcnow(),
		maturityId = MaturitiesJournal.select_last().id
	)

	return redirect("/")


@blueprint.route("/reverse", methods=["GET"])
def reverse():
	SmokesJournal.del_last()

	return redirect("/")


@blueprint.route("/setmaturity", methods=["GET", "POST"])
def set_maturity():
	maturity = MaturitiesJournal.select_last()

	maturityForm = Forms.MaturityForm(request.form)
	if maturityForm.validate_on_submit():
		value = (maturityForm.hours.data * 60) + maturityForm.minutes.data
		MaturitiesJournal.create(value=value)

		return redirect("/")

	return render_template(
		"journals/maturity_page.html",
		maturity = maturity,
		maturityForm = maturityForm
	) 
