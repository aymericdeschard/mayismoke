#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
from datetime import datetime, timedelta
from sqlalchemy import null
from sqlalchemy.sql.expression import func
from Webapp.Database import BaseTable
from Webapp import db

def target_datetime(context):
	maturityId = context.get_current_parameters()['maturity_id'] 	
	sql = """
SELECT
	datetime(sj.value, '+' || mj.value || ' minutes')
FROM
	maturities_journal	mj
	smokes_journal		sj
WHERE
		mj.id = :id
ORDER BY sj.created_date DESC
LIMIT 1
"""
	return db.engine.execute(sql, {"id":"maturityId"})[0]
	

class MaturitiesJournal(db.Model, BaseTable):
	__tablename__ = "maturities_journal"
	value = db.Column("value", db.Integer, nullable=False)

	def __str__(self):
		hours = self.value // 60
		minutes = self.value - (hours * 60)
		return f"{hours:02d}:{minutes:02d}"


class SmokesJournal(db.Model, BaseTable):
	__tablename__ = "smokes_journal"
	value			= db.Column("value",			db.DateTime, nullable=False)
	maturityId		= db.Column("maturity_id",		db.Integer,  db.ForeignKey("maturities_journal.id"), nullable=False)
	targetDatetime	= db.Column("target_datetime",  db.DateTime, nullable=True, default=target_datetime)

	@classmethod
	def create(cls, value, maturityId):
		minutes = MaturitiesJournal.select_by_id(maturityId).value
		lastOccurence = cls.select_last()
		if lastOccurence:
			targetDatetime = lastOccurence.value + timedelta(minutes=minutes)
		else: targetDatetime = null()
		instance = cls(value=value, maturityId=maturityId, targetDatetime=targetDatetime)
		return instance.save()

	@classmethod
	def del_last(cls):
		"""Delete the last entry by creation datetime"""
		cls.select_last().delete()
		return
