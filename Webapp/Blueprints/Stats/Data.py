#!/usr/bin/env python3
# -*- coding: UTF-8 -*-


def chartify(resultSet, labelCol, dataCol, name = "", isReversed=False):
	"""Cast a query set into a understandable structure for
		Chart.js.
		Assume the resultSet is in reversed order.

		Returns dict { 
			"labels": <list> labels of the X axis,
			"data": <list> data points,
			"name": <string> name of the dataset,
			"max" : <integer> maximum value of the serie
		} 

		With:
			resultSet <ResultProxy>	: the entries from SQLAlchemy
			labelCol <String>		: the column name to use as chart's labels
			dataCol <String>		: the column name to use as chart's data
			isReversed <Boolean>	: if the dataset is in reverse order compare to										 the display order in the chart
									  (False by default)
	"""
	if isReversed: direction = -1
	else: direction = 1

	resultSet = list(resultSet)

	data = dict()
	data["labels"]  = [row[labelCol] for row in resultSet[::direction]]
	data["data"]	= [row[dataCol] for row in resultSet[::direction]]
	data["name"]	= name
	data["max"]		= max(data["data"])
	
	return data


def get_daily_cigs(db):
	sql = """
SELECT
	STRFTIME('%Y-%m-%d', created_datetime)  AS date,
	COUNT(id)								AS count
FROM
	smokes_journal
GROUP BY STRFTIME('%Y-%m-%d', created_datetime)
ORDER BY date DESC
LIMIT :limit
"""
	return db.engine.execute(sql, limit=14)


def get_actuals_targets_diff(db):
	sql = """
/* 
	Calculate the difference between 
	the target and the actual
*/
WITH diff AS (
	SELECT
		sj.id,
		(STRFTIME('%s', sj.value) - STRFTIME('%s', sj.target_datetime))/60 AS diff
	FROM
		smokes_journal	sj
	WHERE
		sj.target_datetime IS NOT NULL
),
/*
	Calculate the percentage threshold to eliminate
	outliers data (due to the user not smoking while
	sleeping).
*/
threshold AS (
	SELECT
		diff.id,
		diff.diff / (avg(diff.diff) over())  AS val
	FROM
		diff
)

SELECT
	sj.id,
	diff.diff								AS diff,
	STRFTIME('%Y-%m-%d %H:%M', sj.value) 	AS label,
	threshold.val AS threshold
	
FROM
	smokes_journal							sj
	JOIN diff
	ON diff.id = sj.id
	JOIN threshold
	ON threshold.id = sj.id
WHERE
	ABS(threshold.val) < 0.3
ORDER BY sj.value DESC
LIMIT :limit
"""
	return db.engine.execute(sql, limit=14)


def get_maturity_evol(db):
	sql = """
SELECT
	value AS maturity,
	STRFTIME('%Y-%m-%d', created_datetime) AS label
FROM
	maturities_journal
LIMIT :limit
"""

	return db.engine.execute(sql, limit=14)


def get_present_previous_diff(db):
	sql = """
/* Calculate the difference between the present smoked and the previous record
in minutes */
WITH diff AS (
	SELECT
		row_number() OVER (ORDER BY sj.id) AS id,
		(STRFTIME('%s', sj.value) - STRFTIME('%s', sj_previous.value))/60 AS diff

	FROM
		smokes_journal	sj
		JOIN smokes_journal sj_previous
		ON sj_previous.id = (SELECT sj3.id
							FROM smokes_journal sj3
							WHERE
								sj3.id < sj.id
							ORDER BY sj3.id DESC
							LIMIT 1)
),

/* Calculate the percentage threshold to eliminate outliers data 
(due to the user not smoking while sleeping). */
threshold AS (
	SELECT
		diff.id AS id,
		diff.diff / (avg(diff.diff) over())  AS val
	FROM
		diff
)

SELECT
	diff.id AS id,
	diff.diff AS diff,
	diff.id AS label
FROM
	diff
	JOIN threshold
	ON threshold.id = diff.id
	AND threshold.val < 0.3
ORDER BY diff.id DESC
LIMIT :limit
"""

	return db.engine.execute(sql, limit=14)
