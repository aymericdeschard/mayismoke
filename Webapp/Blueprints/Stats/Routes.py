#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
from datetime import datetime, timedelta
import json

from flask import Blueprint, render_template, redirect, request

import Settings
from Webapp import db
from .Data import (
		chartify,
		get_daily_cigs,
		get_actuals_targets_diff,
		get_maturity_evol,
		get_present_previous_diff
	)

blueprint = Blueprint(
	"stats",
	__name__,
	template_folder="templates"
	)


@blueprint.route("/", methods=["GET"])
def index():
	dataDailyConsumption = [
		chartify(get_daily_cigs(db), "date", "count", isReversed=True)
	]

	dataActualVsTarget = [
		chartify(get_actuals_targets_diff(db), "label", "diff", isReversed=True),
	]


	dataMaturityEvol = [
		chartify(get_maturity_evol(db), "label", "maturity")
	]

	return render_template(
		"stats/index.html",
		dataDaily = dataDailyConsumption,
		dataActualVsTarget = dataActualVsTarget,
		dataMaturityEvol = dataMaturityEvol
	)

