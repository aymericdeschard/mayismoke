#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from datetime import datetime
from sqlalchemy.sql.expression import func
from Webapp import db

class BaseTable():
	id				= db.Column("id",				db.Integer,		primary_key=True)
	createdDateTime	= db.Column("created_datetime", db.DateTime,	nullable=False, default=datetime.utcnow)
	# creatorId		= db.Column("creator_id",		db.Integer,		nullable=False)
	updatedDateTime	= db.Column("updated_datetime", db.DateTime,	nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
	# updaterId		= db.Column("updater_id,		db.Integer,		nullable=False)

	@classmethod
	def create(cls, **kwargs):
		"""Create a table entry."""
		instance = cls(**kwargs)
		return instance.save()

	def update(self, commit=True, **kwargs):
		"""Update specific fields of an entry."""
		for attr, value in kwargs.items():
			setattr(self, attr, value)
		return commit and self.save() or self

	def save(self, commit=True):
		"""Save the entry."""
		db.session.add(self)
		if commit:
			db.session.commit()
		return self

	def delete(self, commit=True):
		"""Remove the entry from the database."""
		db.session.delete(self)
		return commit and db.session.commit()

	@classmethod
	def select_last(cls):
		"""Return the last entry in the table."""
		maxDateQuery = db.session.query(func.max(cls.createdDateTime))
		return cls.query.filter(cls.createdDateTime == maxDateQuery).one()

	@classmethod
	def select_by_id(cls, id):
		"""Return a record given by its id."""
		return cls.query.get(id)
