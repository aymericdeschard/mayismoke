#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

try:
	from flask import Flask
except ModuleNotFoundError:
	print("A dependency is missing. Run: pip install -r requirements.txt")
	exit(1)
	
from .Extensions import db, migrate, csrfProtect
from .Blueprints import register_blueprints, register_models

import Settings

def create_app():
	app = Flask(__name__)
	app.config.from_object(Settings)

	register_extensions(app)
	# register_models()	
	register_blueprints(app)

	return app


def register_extensions(app):
	db.init_app(app)
	with app.app_context():
		if db.engine.url.drivername == 'sqlite':
			migrate.init_app(app, db, render_as_batch=True)
		else:
			migrate.init_app(app, db)	
	app.jinja_env.trim_blocks = True
	app.jinja_env.lstrip_blocks = True	
	csrfProtect.init_app(app)
