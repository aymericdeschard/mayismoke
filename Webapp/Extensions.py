#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
try:

	from flask_sqlalchemy import SQLAlchemy
	from flask_migrate import Migrate
	from flask_wtf.csrf import CSRFProtect

except ModuleNotFoundError:
	print("A dependency is missing. Run: pip3 install -r requirements.txt")
	exit(1)

db = SQLAlchemy()
migrate = Migrate(compare_type=True)
csrfProtect = CSRFProtect()
